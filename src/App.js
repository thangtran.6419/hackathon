import React from "react";

import { Router, Route } from "./components/react-router";
import { COMP } from './screens';
import { Provider } from 'mobx-react';
import * as Store from './stores'
import { View } from 'react-native-web'

console.log('dfssdfdf')
const App = () => (
  <Provider store={Store}>
    <View style = {{flex: 1}}>
      <Router>
        <Route exact path="/" component={COMP.MAIN} />
        <Route path="/ball" component={COMP.BALL} />
        <Route path="/suggest" component={COMP.DETAIL_SUG} />
        <Route path="/form" component={COMP.FORM} />
        <Route path="/demo" component={COMP.DEMO} />
        <Route path="/home" component={COMP.HOME} />
        <Route path="/search" component={COMP.SEARCH} />
        <Route path="/orderdetail/:bill_code" component={COMP.ORDER_DETAIL} />
        <Route path="/transaction" component={COMP.TRANSACTION} />
      </Router>
    </View>
  </Provider>
);

export default App;