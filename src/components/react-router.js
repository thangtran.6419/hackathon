import { BrowserRouter, Route as WebRoute, Link as WebLink, HashRouter } from 'react-router-dom'

export let Router = HashRouter;
export let Route = WebRoute;
export let Link = WebLink;
