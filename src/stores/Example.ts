import { observable } from 'mobx';

class Example {
  @observable kaki = 1;
  @observable avatar = '';
  @observable phone_number = '';
  @observable fullname = '';
}

export default new Example();