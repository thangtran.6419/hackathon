interface UserInfo {
  accountType: "user" | "merchant",
  address: string,
  avatar: string,
  balance: number,
  birthday: string,
  cmndNumber: string,
  connectFb: boolean,
  email: string,
  fullname: string,
  gender: "male" | "female",
  payVerifyMethod: "NONE" | "OTP",
  phoneNumber: string,
  userId: number
  username: string,
  vip: number,
}

interface OrderInfo {
  api_key: string,
  amount: number,
  order_id: string,
  order_info: string,
}

interface ShareProp {
  title: string,
  message: string,
}

interface SchemaProp {
  schema: string,
}

export interface DeviceInfo {
  device_id: string;
  device_name: string;
  device_platform: string;
  bundle_id: string;
  device_os_version: string;
  is_tablet: boolean;
  ip_address: string;
  battery_level: number;
  battery_changing: boolean;
  device_mac_address: string;
  device_manufacturer: string;
  device_brand: string;
  wifi_name: string;
  is_wifi: boolean;
  has_network: string;
  is_mobile_data: boolean;
}
interface AppotaSDKOptions {
  title?: string;
}

declare class AppotaSDK {
  static sdk_ready: boolean;
  static init(options?: AppotaSDKOptions): void;
  static validInitSDK(): void;
  static setHeaderTitle(title: string): Promise<any>;
  // static setItem(key: string, value: string): Promise<boolean>;
  // static getItem(key: string): Promise<string>;
  // static getDeviceInfo(fields: Array<keyof DeviceInfo>): Promise<DeviceInfo>;
  static readBarCode(): Promise<string>;
  static getUserInfo(): Promise<UserInfo>;
  static showPopupInput(title: string, description: string): Promise<string>;
  static requestPay(order: OrderInfo): Promise<string>;
  static setClipboard(data: string): void;
  static share(data: ShareProp): Promise<any>;
  static openSchema(data: SchemaProp): void;
}
export { AppotaSDK };
