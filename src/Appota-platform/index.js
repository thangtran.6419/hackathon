"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AppotaSDK = (function () {
  function AppotaSDK() {
  }
  AppotaSDK.init = function (options) {
    if (options === void 0) { options = {}; }
    if (typeof window.Appota.handleSDK !== "function") {
      throw new Error("Bạn phải sử dụng sdk trong ứng dụng Appota");
    }
    AppotaSDK.sdk_ready = true;
    window.Appota.handleSDK("init", {
      fields: {
        title: options.title || null,
      }
    }).catch();
  };
  AppotaSDK.validInitSDK = function () {
    if (!AppotaSDK.sdk_ready) {
      throw new Error("SDK chưa được khởi tạo, vui lòng gọi init()");
    }
  };
  AppotaSDK.setHeaderTitle = function (title) {
    AppotaSDK.validInitSDK();
    return window.Appota.handleSDK("setHeaderTitle", {
      fields: {
        title: title
      }
    });
  };
  AppotaSDK.readBarCode = function () {
    AppotaSDK.validInitSDK();
    return window.Appota.handleSDK("readBarCode");
  };
  AppotaSDK.getUserInfo = function () {
    AppotaSDK.validInitSDK();
    return window.Appota.handleSDK("getUserInfo");
  }
  AppotaSDK.getAccessToken = function () {
    AppotaSDK.validInitSDK();
    return window.Appota.handleSDK("getAccessToken");
  }
  AppotaSDK.showPopupInput = function (title, description) {
    AppotaSDK.validInitSDK();
    return window.Appota.handleSDK("showPopupInput", {
      fields: {
        title,
        description,
      }
    });
  };
  AppotaSDK.requestPay = function ({ api_key, amount, order_id, order_info }) {
    AppotaSDK.validInitSDK();
    return window.Appota.handleSDK("requestPay", {
      fields: {
        api_key,
        amount,
        order_id,
        order_info
      }
    });
  };
  AppotaSDK.setClipboard = function (data) {
    AppotaSDK.validInitSDK();
    return window.Appota.handleSDK("setClipboard", {
      fields: {
        data
      }
    });
  }
  AppotaSDK.share = function ({ title, message }) {
    AppotaSDK.validInitSDK();
    return window.Appota.handleSDK("share", {
      fields: {
        title, message
      }
    });
  }
  AppotaSDK.openSchema = function ({ schema }) {
    AppotaSDK.validInitSDK();
    return window.Appota.handleSDK("openSchema", {
      fields: { schema }
    });
  }
  // AppotaSDK.verifyTransaction = function () {
  //   AppotaSDK.validInitSDK();
  //   return window.Appota.handleSDK('verifyTransaction')
  // }
  AppotaSDK.sdk_ready = false;
  return AppotaSDK;
}());
exports.AppotaSDK = AppotaSDK;
