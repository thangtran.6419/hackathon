const crypto256 = require('crypto-js/sha256');
const qs = require('qs');
const HOST = 'http://192.168.187.157:8001';
const SECRET_KEY = 'qevBaXyB6r4T3kQW29kvAdVFWb9RZ6';

class RequestHelper {

  makeSignature = (obj) => {
    let skey = SECRET_KEY;
    let args = Object.keys(obj).sort().map((k) => { return obj[k]; });
    let signature = crypto256(args.join('') + skey).toString();
    return Object.assign(obj, { signature });
  }

  stringify = (obj, contentType) => {
    switch (contentType) {
      case 'json': return JSON.stringify(obj);
      case 'application/json': return JSON.stringify(obj);
      case 'application/x-www-form-urlencoded': return qs.stringify(obj);
      case 'urlencoded': return qs.stringify(obj);
      default: return JSON.stringify(obj);
    }
  }

  querify = (url, queryObject) => {
    let newUrl = url;
    if (queryObject === undefined) return newUrl;
    newUrl += `?${qs.stringify(queryObject)}`;
    return newUrl;
  }

  get = async ({ url, data = undefined }) => {
    try {
      let DOMAIN = HOST;
      const apiUrl = this.querify(DOMAIN + url, data === undefined ? this.makeSignature({}) : this.makeSignature(data));
      const response = await fetch(apiUrl, {
        headers: {
          "Sec-Fetch-Mode": "cors",
          "client-authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZV9udW1iZXIiOiIwOTg5NzQyMzkyIn0.JocF_leVdkDVQJcpHMPtP0ADOmEvfjmc2iOV331uAVM",
        },
      });
      return response;
    } catch (err) {
      console.log(err);
    }
  }

  post = async ({ url, data }) => {
    try {
      let DOMAIN = HOST;
      const response = await fetch(DOMAIN + url, {
        method: 'POST',
        headers: {
          "Sec-Fetch-Mode": "cors",
          "client-authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZV9udW1iZXIiOiIwOTg5NzQyMzkyIn0.JocF_leVdkDVQJcpHMPtP0ADOmEvfjmc2iOV331uAVM",
          'content-type': 'application/x-www-form-urlencoded'
        },
        body: this.stringify(this.makeSignature(data), 'application/x-www-form-urlencoded'),
      });
      return response;
    } catch (err) {
      console.log(url, err);
    }
  }

  delete = async ({ url, data }) => {
    try {
      let DOMAIN = HOST;
      const response = await fetch(DOMAIN + url, {
        method: 'DELETE',
        headers: {
          "Sec-Fetch-Mode": "cors",
          "client-authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZV9udW1iZXIiOiIwOTg5NzQyMzkyIn0.JocF_leVdkDVQJcpHMPtP0ADOmEvfjmc2iOV331uAVM",
        },
        // body: this.stringify(this.makeSignature(data), 'json'),
      });
      return response;
    } catch (err) {
      console.log(url, err);
    }
  }

}

export default new RequestHelper();
