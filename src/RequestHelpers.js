export const getListTransaction = async () => {
  try {
    const res = await fetch(`https://miniapp-order.ewallet.appota.com/transactions`)
    let json = await res.json();
    return json;
  } catch (err) {
    console.log(err);
  }
}

export const requestOrder = async ({ bill_code }) => {
  try {
    let data = {
      bill_code
    }
    const res = await fetch(`https://miniapp-order.ewallet.appota.com/payment_order/request`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Sec-Fetch-Mode': 'cors',
      },
      body: JSON.stringify(data)
    })
    let json = await res.json();
    return json;
  } catch (err) {
    console.log(err);
  }
}

export const confirmOrder = async ({ order_id, transaction_id }) => {
  try {
    let data = {
      order_id, transaction_id
    }
    const res = await fetch(`https://miniapp-order.ewallet.appota.com/payment_order/confirm`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Sec-Fetch-Mode': 'cors',
      },
      body: JSON.stringify(data)
    })
    let json = await res.json();
    return json;
  } catch (err) {
    console.log(err);
  }
}