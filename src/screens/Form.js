import React from 'react';
import { View, StyleSheet, Dimensions, Text, Image, FlatList, TouchableOpacity } from 'react-native-web';
import { AppotaSDK } from '../Appota-platform';
import { withRouter } from 'react-router-dom';
import { COLOR } from '../constants'
import ListItem from './component/ListItem';
import Popup from './component/Popup';
import { Example } from '../stores';
import Request from '../Request'

const { height, width } = Dimensions.get('window');

class Main extends React.Component {
  state = {
    showPopup: true,
    text: '',
    indexS: 1000
  }

  data = [
    { avatar: require('../assets/a1.webp'), name: 'Tran Van Thang 1', time: 4, money: 200 },
    { avatar: require('../assets/a1.webp'), name: 'Tran Van Thang 2', time: 3, money: 150 },
    { avatar: require('../assets/a1.webp'), name: 'Tran Van Thang 3', time: 2, money: 100 },
    { avatar: require('../assets/a1.webp'), name: 'Tran Van Thang 4', time: 1, money: 50 },
    { avatar: require('../assets/a1.webp'), name: 'Tran Van Thang 4', time: 1, money: 50 },
    { avatar: require('../assets/a1.webp'), name: 'Tran Van Thang 4', time: 1, money: 50 },
    { avatar: require('../assets/a1.webp'), name: 'Tran Van Thang 4', time: 1, money: 50 },
    { avatar: require('../assets/a1.webp'), name: 'Tran Van Thang 4', time: 1, money: 50 },
    { avatar: require('../assets/a1.webp'), name: 'Tran Van Thang 4', time: 1, money: 50 },
  ]
  componentDidMount = () => {
    this.getListOption()
    this.initInterval = setInterval(() => {
      if (window.Appota) {
        AppotaSDK.init({ title: 'Thăm giò ý kiến' })
        clearInterval(this.initInterval)
      }
    }, 50);
  }

  toDetail = (item) => {
    this.props.history.push({
      pathname: `/transaction`, state: {}
    })
  }

  sendRequest = async () => {
    const data = {
      phone_number: Example.phone_number, option_id: 1, poll_id: ''
    }
    // this.props.history.push({
    //   pathname: `/suggest`, state: {}
    // })
    let res = await Request.post({ url: "/v1/poll/create", data });
    let json = res ? await res.json() : "";
    if (!json) return
    this.props.history.push({
      pathname: `/suggest`, state: {}
    })
    return json;
  }

  getListOption = async () => {
    const data = {
      poll_id: ''
    }
    let res = await Request.get({ url: "/v1/poll/get", data });
    let json = res ? await res.json() : '';
    if (!json) return
    console.log(json)
    return json;
  }

  renderItem = (item, index) => {
    let { indexS } = this.state;
    return <TouchableOpacity key={`main${index}`} onPress={() => this.setState({ indexS: index })} style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 10, paddingVertical: 10, paddingHorizontal: 20, borderRadius: 20, backgroundColor: COLOR.WHITE }}>
      <View style={{ flex: 1, display: 'flex', flexWrap: 'wrap' }}>
        <Text style={{ fontSize: 18, color: COLOR.MAIN }}>Lẩu Phan</Text>
        <Text style={{ fontSize: 15, color: COLOR.MAIN_TEXT }}>Số 42 ngõ 67 Láng Hạ, Ba Đình, Hà Nội Viet Nam abcxyz</Text>
      </View >
      {indexS == index ? <Image source={require('../assets/star.png')} style={{ width: 40, height: 40 }} /> : <View style={{ width: 40 }} />}
    </TouchableOpacity>
  }

  render() {
    const { avatar, phone_number, fullname } = Example
    return (
      <View style={{ flex: 1, backgroundColor: COLOR.BG, width, minHeight: height, display: 'flex', flexWrap: 'wrap', paddingHorizontal: 15 }}>
        <View style={{ marginVertical: 20, padding: 20, borderRadius: 20, backgroundColor: COLOR.WHITE, alignItems: 'center', flexDirection: 'row' }}>
          <Image source={avatar ? { uri: avatar } : require('../assets/a1.jpg')} style={{ width: 80, height: 80, borderRadius: 40, marginRight: 20 }} />
          <View>
            <Text style={{ color: COLOR.RED, fontWeight: 'bold', fontSize: 18 }}>{fullname ? fullname : '000000000'}</Text>
            <Text style={{ color: COLOR.MAIN_TEXT, fontSize: 15 }}>{phone_number ? phone_number : '000000000'}</Text>
          </View>
        </View>
        <View style={{ flex: 1, paddingVertical: 15, borderRadius: 20 }}>
          <Text style={{ color: COLOR.MAIN_TEXT, fontSize: 18, marginBottom: 15, marginLeft: 5 }}>Nhậu đâu anh em êy!?</Text>
          {this.data.map((item, index) => this.renderItem(item, index))}
        </View>
        <TouchableOpacity onPress={this.sendRequest} style={{ paddingVertical: 10, borderRadius: 15, margin: 10, alignItems: 'center', backgroundColor: COLOR.RED }}>
          <Text style={{ color: COLOR.WHITE, fontSize: 16, fontWeight: 'bold' }}>Gửi yêu cầu</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
export default withRouter(Main)
const styles = StyleSheet.create({
});
