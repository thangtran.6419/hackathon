import React from 'react';
import { Alert, View, StyleSheet, Dimensions, Text, TouchableOpacity, ActivityIndicator } from 'react-native-web';
import { AppotaSDK } from '../Appota-platform';
import { requestOrder, getListTransaction, confirmOrder } from '../RequestHelpers';

const { height, width } = Dimensions.get('window');

export default class History extends React.Component {
  state = {
    isLoading: true,
    billInfo: {
      address: '',
      amount: 0,
      bill_code: '',
      fullname: '',
      service_code: ''
    },
    error_message: '',
    order_id: '',
    payment_comfirm: false
  }

  componentDidMount() {
    this.requestOrder();
  }

  requestOrder = async () => {
    let res = await requestOrder({ bill_code: this.props.match.params.bill_code});
    this.setState({
      isLoading: false
    })
    if (res.billInfo) {
      this.setState({
        billInfo: res.billInfo,
        order_id: res.order_id
      })
    } else {
      this.setState({
        error_message: res.error.message,
      })
    }
  }

  confirmOrder =  async (transaction_id) => {
    const { order_id } = this.state;
    let res = await confirmOrder({ order_id, transaction_id });
    if(res.success) {
      this.setState({
        payment_comfirm: true
      })
    } else {
      alert(res.error.message)
    }
  }

  onPayment = async () => {
    const { billInfo, order_id } = this.state;
    AppotaSDK.requestPay({
      api_key: '8quWx2tvrmQgdbNVnCuU',
      amount: billInfo.amount,
      order_id,
      order_info: billInfo.bill_code,
    })
      .then((a) => this.confirmOrder(a))
      .catch(e => alert(e.toString()))
  }

  goBack = () => {
    this.props.history.goBack()
  }

  render() {
    const { billInfo, isLoading, error_message, payment_comfirm } = this.state;
    return (
      <View style={{ flex: 1, width, minHeight: height, backgroundColor: 'white', display: 'flex' }}>
        <View style={{ flex: 1, padding: 15 }}>
          <Text style={{ color: 'black', fontSize: 14, fontWeight: '600' }}>
            THÔNG TIN HÓA ĐƠN
          </Text>
          {isLoading ?
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <ActivityIndicator color="black" />
            </View>
            :
            <View style={styles.info}>
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ color: 'green', fontSize: 30, fontWeight: '600' }}>
                  {billInfo.amount}
                  <Text style={{ color: 'green', fontSize: 13, fontWeight: '600' }}>
                    vnđ
                </Text>
                </Text>
              </View>
              <Text style={styles.title}>
                Mã hóa đơn:
              <Text style={styles.txtinfo}>
                  {` ${billInfo.bill_code}`}
                </Text>
              </Text>
              <Text style={styles.title}>
                Mã dịch vụ:
              <Text style={styles.txtinfo}>
                  {` ${billInfo.service_code}`}
                </Text>
              </Text>
              <Text style={styles.title}>
                Họ và tên:
              <Text style={styles.txtinfo}>
                  {` ${billInfo.fullname}`}
                </Text>
              </Text>
              <Text style={styles.title}>
                Địa chỉ:
                <Text style={styles.txtinfo}>
                  {` ${billInfo.address}`}
                </Text>
              </Text>
            </View>
          }
          {error_message.length > 0 &&
            <View style={{ borderRadius: 5, backgroundColor: '#F2DEDF', padding: 10, marginTop: 10 }}>
              <Text style={{ color: '#8B3E48', fontSize: 13, marginTop: 2 }}>
                {`• ${error_message}`}
              </Text>
            </View>
          }
          {payment_comfirm &&
            <Text style={{ color: 'green', fontSize: 15, marginTop: 15, textAlign: 'center', fontWeight: 'bold' }}>
              THÀNH CÔNG
            </Text>
          }
        </View>
        <View style={{ flexDirection: 'row', padding: 15 }}>
          <TouchableOpacity onPress={this.goBack} style={styles.button}>
            <Text style={{ color: 'black', fontSize: 14, fontWeight: 'bold' }}>
              Hủy
            </Text>
          </TouchableOpacity>
          {!payment_comfirm &&
            <View style={{ width: 15 }} />
          }
          {!payment_comfirm &&
            <TouchableOpacity onPress={this.onPayment} style={styles.button}>
              <Text style={{ color: 'green', fontSize: 14, fontWeight: 'bold' }}>
                Thanh toán
              </Text>
            </TouchableOpacity>
          }
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  info: {
    backgroundColor: 'white',
    alignSelf: 'center',
    borderRadius: 10,
    shadowColor: 'black',
    shadowRadius: 5,
    shadowOpacity: 0.1,
    shadowOffset: { height: 1, width: 0 },
    marginTop: 15,
    width: '100%',
    padding: 15,
    justifyContent: 'center'
  },
  button: {
    height: 40,
    borderRadius: 7,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.1)'
  },
  title: {
    fontSize: 14,
    marginTop: 10
  },
  txtinfo: {
    fontSize: 14,
    color: 'green'
  }
});
