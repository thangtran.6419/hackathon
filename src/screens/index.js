import Home from './Home'
import OrderDetail from './OrderDetail'
import Transaction from './Transaction'
import Demo from './Demo'
import Ball from './Ball'
import Main from './Main'
import Search from './Search'
import Form from './Form'
import DetailRemind from './DetailRemind'

export const COMP = {
  HOME: Home,
  MAIN: Main,
  ORDER_DETAIL: OrderDetail,
  TRANSACTION: Transaction,
  DEMO: Demo,
  BALL: Ball,
  SEARCH: Search,
  FORM: Form,
  DETAIL_SUG: DetailRemind
}
