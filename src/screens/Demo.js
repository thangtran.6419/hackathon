import React from 'react';
import { View, StyleSheet, Dimensions, Text, Animated, Image, Easing } from 'react-native-web';
import { AppotaSDK } from '../Appota-platform';
import { withRouter } from 'react-router-dom';

const { height, width } = Dimensions.get('window');
const arr = Array.from(new Array(500), (x, i) => i + 1)
class Transaction extends React.Component {
  constructor() {
    super()
    this.spinValue = new Animated.Value(0)
    this.springValue = new Animated.Value(0.3)
    this.animatedValue = []
    arr.map(val => this.animatedValue[val] = new Animated.Value(0))
  }
  state = {
    isLoading: true,
    value: 1
  }

  componentDidMount = () => {
    // this.spin()
    this.spring()
    // this.animated()
  }

  // animated = () => {
  //   // alert('hoho')
  //   const animations = arr.map(i => {
  //     return Animated.timing(
  //       this.animatedValue[i], {
  //         toValue: 1,
  //         duration: 4000
  //       }
  //     )
  //   })
  //   Animated.se(animations).start()
  // }

  spring = () => {
    this.springValue.setValue()
    Animated.spring(
      this.springValue, {
        toValue: this.state.value,
        friction: 1,
        duration: 5000
      }
    ).start(() => {
      this.setState({ value: this.state.value + 1 }, () => this.spring())
    })
  }

  spin = () => {
    this.spinValue.setValue(0)
    Animated.timing(
      this.spinValue, {
        toValue: 1,
        duration: 4000,
        easing: Easing.linear
      }
    ).start(this.spin)
  }

  render() {
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    })
    const marginLeft = this.spinValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 300, 0]
    })
    const opacity = this.spinValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 1, 0]
    })
    const movingMargin = this.spinValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 300, 0]
    })
    const textSize = this.spinValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [18, 32, 18]
    })
    const rotateX = this.spinValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: ['0deg', '180deg', '0deg']
    })
    const animation = arr.map((item, index) => {
      return <Animated.View
        key = {`saf${index}`}
        style = {{ opacity: this.animatedValue[item], height: 20, width: 20, margin: 1.5, backgroundColor: 'blue'}}
      />
    })
    return (
      <View style={{ flexWrap: 'wrap', flex: 1, width, minHeight: height, backgroundColor: 'red', display: 'flex', flexDirection: 'row', marginVertical: 15 }}>
        {/* <Animated.Image
          style={{
            width: 100, height: 100,
            transform: [{ scale: this.springValue }],
            // marginLeft: marginLeft,
            // opacity: opacity
          }}
          source={{ uri: "https://static.appotapay.com//assert/fr/2020-02-13/k4PYiMR4LSHKiMyU07QkVWZyBajQoPZ4hvRMNYj2.png" }}
        /> */}
        {animation}
      </View>
    )
  }
}
export default withRouter(Transaction)
const styles = StyleSheet.create({
});
