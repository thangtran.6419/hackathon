import React from 'react';
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native-web';
import { AppotaSDK } from '../Appota-platform';
import { withRouter } from 'react-router-dom';

const { height, width } = Dimensions.get('window');

class Home extends React.Component {
  state = {
    isLoading: true,
    avatar: '',
    phone_number: '',
    fullname: ''
  }

  componentDidMount() {
    // setup SDK;
    // this.initInterval = setInterval(() => {
    //   if (window.Appota) {
    //     AppotaSDK.init({ title: 'Test miniapp' })
    //     this.setState({ isLoading: false }, () => {
    //       this.getUserInfo()
    //     })
    //     clearInterval(this.initInterval)
    //   }
    // }, 50);
  }

  requestCamera = async () => {
    let data = await AppotaSDK.readBarCode();
    if (data) {
      if (data.indexOf('miniapp://') == 0) {
        let [part1, part2] = data.split('?');
        let [screenName] = part1.replace(/.*?:\/\//g, '').split('/');
        let params = part2.split('&');
        if (!screenName) return;
        screenName = screenName.toUpperCase();
        let props = {}
        if (params && params.length > 0) {
          for (let i = 0; i < params.length; i++) {
            if (!params[i] || params[i].indexOf('=') < 0) continue;
            let propName = params[i].split('=')[0];
            let propValue = params[i].split('=')[1];
            props[propName] = propValue;
          }
        }
        switch (screenName) {
          case 'ORDER_SERVICE':
            this.navigateToOrderDetail(props.bill_id)
            break;
          default:
            alert(data);
            break;
        }
      } else {
        alert(typeof data == 'string' ? data : JSON.stringify(data));
      }
    }
  }

  getUserInfo = async () => {
    let data = await AppotaSDK.getUserInfo();
    this.setState({
      avatar: data.avatar,
      phone_number: data.phoneNumber,
      // fullname: data.fullname
    })
  }

  verifyTransaction = () => {
    AppotaSDK.verifyTransaction()
      .then((a) => alert('verify success'))
      .catch(e => alert(e.toString()))
  }

  navigateToOrderDetail = (bill_code) => {
    this.props.history.push({pathname:`/home`, state: { alo: 1 }})
  }

  showPopupInput = async () => {
    let res = await AppotaSDK.showPopupInput('Title test', 'description');
    this.navigateToOrderDetail(res)
  }

  copyText = async () => {
    AppotaSDK.setClipboard("HELLO")
  }

  shareContent = async () => {
    AppotaSDK.share({ title: 'Hello', message: 'From MiniApp Appota' }).then(() => {
      alert('Share done')
    })
  }

  render() {
    const { isLoading, fullname, phone_number, avatar } = this.state;
    return (
      <View style={{ flex: 1, width, minHeight: height, backgroundColor: 'white', display: 'flex' }}>
        {/* {isLoading ? <ActivityIndicator color="white" /> :
          <TouchableOpacity onPress={this.verifyTransaction}>
            <Image source={require('./logo.svg')} style={{ width: width / 3, height: width / 3 }} />
          </TouchableOpacity>
        } */}
        <View style={{ flexDirection: 'row', margin: 15 }}>
          <View style={{ height: 80, width: 80, borderRadius: 40, backgroundColor: 'rgba(0,0,0,0.3)' }}>
            <Image
              source={{ uri: avatar }}
              style={{ height: 80, width: 80, borderRadius: 40 }}
            />
          </View>
          <View style={{ flex: 1, marginLeft: 10, marginTop: 5 }}>
            <Text style={{ color: 'black', fontSize: 16, fontWeight: 'bold' }}>
              {fullname || "Yoyo"}
            </Text>
            <Text style={{ color: 'black', fontSize: 14, fontWeight: '500' }}>
              {phone_number || "0354857493"}
            </Text>
          </View>
        </View>
        <View style={{ flex: 1, paddingHorizontal: 15 }}>
          <TouchableOpacity style={styles.button} onPress={this.navigateToOrderDetail}>
            <Text style={styles.txtButton}>
              Quét QR Code
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={this.showPopupInput}>
            <Text style={styles.txtButton}>
              Thanh toán hóa đơn
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={this.copyText}>
            <Text style={styles.txtButton}>
              Copy text
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={this.shareContent}>
            <Text style={styles.txtButton}>
              Share content
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export default withRouter(Home);

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'white',
    alignSelf: 'center',
    borderRadius: 10,
    shadowColor: 'black',
    shadowRadius: 5,
    shadowOpacity: 0.1,
    shadowOffset: { height: 1, width: 0 },
    marginTop: 15,
    height: 50,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  txtButton: {
    fontSize: 15,
    fontWeight: '600',
  }
});
