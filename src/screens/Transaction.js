import React from 'react';
import { View, StyleSheet, Dimensions, Text, Image, ScrollView, TouchableOpacity, ActivityIndicator } from 'react-native-web';
import { AppotaSDK } from '../Appota-platform';
import { withRouter } from 'react-router-dom';
import { Pie, Bar } from 'react-chartjs-2';
import { COLOR } from '../constants';
import Request from '../Request'

const { height, width } = Dimensions.get('window');
const data = {
  labels: [
    'Bạn',
    'Team',
  ],
  datasets: [{
    data: [250, 1500],
    backgroundColor: [
      '#FF6384',
      '#36A2EB',
    ],
    hoverBackgroundColor: [
      '#FF6384',
      '#36A2EB',
    ]
  }]
};

const dataBar = {
  labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13'],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: 'red',
      borderColor: 'blue',
      borderWidth: 1,
      hoverBackgroundColor: 'green',
      hoverBorderColor: 'yellow',
      data: [65, 59, 80, 81, 56, 55, 40, 0, 16, 22, 35, 65, 42]
    }
  ]
};

class Transaction extends React.Component {
  state = {
    isLoading: true,
    stateT: ''
  }


  componentDidMount = () => {
    if (!this.props.history.location.state) this.back()
    this.setState({ stateT: this.props.history.location.state })
    this.initInterval = setInterval(() => {
      if (window.Appota) {
        AppotaSDK.init({ title: 'Thông tin chi tiết' })
        clearInterval(this.initInterval)
      }
    }, 50);
  }

  back = () => {
    this.props.history.push({
      pathname: `/`,
    })
  }

  edit = (data) => {
    this.props.history.push({
      pathname: `/search`, state: { isEdit: true, data: data }
    })
  }

  delete = async (user) => {
    const data = {
      phone_number: user.phone_number, user_id: ''
    }
    let res = await Request.post({ url: "/v1/user/delete", data });
    let json = res ? await res.json() : "";
    if (!json) return
    if (json.success) this.props.history.push(`/`)
    return json;
  }

  itemInfo = (title, content) => {
    return <View style={{ marginHorizontal: 20, marginBottom: 10, paddingVertical: 10, paddingHorizontal: 20, borderRadius: 20, backgroundColor: COLOR.WHITE }}>
      <Text style={{ fontSize: 15, color: COLOR.MAIN_TEXT }}>{title}: {content}</Text>
    </View>
  }

  render() {
    if (!this.props.history.location.state) return <View />
    const { state } = this.props.history.location
    const { avatar, user_name, time, money, phone_number } = state.item
    return (
      <View style={{ flex: 1, width, minHeight: height, display: 'flex', flexWrap: 'wrap', backgroundColor: COLOR.BG, padding: 15 }}>
        <View style={{ marginBottom: 10, padding: 20, borderRadius: 20, backgroundColor: COLOR.WHITE, alignItems: 'center', justifyContent: 'center' }}>
          <Image source={require('../assets/bg.png')} style={{ width: '100%', height: 60 }} />
          <Image source={require('../assets/dragon.png')} style={{ width: 100, height: 100, borderRadius: 40, marginTop: -15 }} />
          <Text style={{ fontSize: 24, fontWeight: 'bold', color: COLOR.RED, marginVertical: 10 }}>No.{state.index}</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          {this.itemInfo('Họ - tên', user_name)}
          {this.itemInfo('Số điện thoại', phone_number)}
          {this.itemInfo('Số lần đi muộn', time)}
          {this.itemInfo('Số tiền phải đóng', money)}
          <Pie data={data} />
          <View style={{ marginVertical: 10, flexDirection: 'row' }}>
            <TouchableOpacity onPress={() => this.edit(state.item)} style={{ paddingVertical: 10, borderRadius: 15, flex: 1, marginHorizontal: 10, alignItems: 'center', backgroundColor: COLOR.GOOD_DAY }}>
              <Text style={{ color: COLOR.WHITE, fontSize: 16 }}>Sửa</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.delete(state.item)} style={{ paddingVertical: 10, borderRadius: 15, flex: 1, marginHorizontal: 10, alignItems: 'center', backgroundColor: COLOR.RED }}>
              <Text style={{ color: COLOR.WHITE, fontSize: 16 }}>Xóa</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View >
    )
  }
}
export default withRouter(Transaction)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
});
