import React from 'react';
import { View, StyleSheet, Dimensions, Text, Image, ScrollView } from 'react-native-web';
import { AppotaSDK } from '../Appota-platform';
import { withRouter } from 'react-router-dom';
import { Pie, Bar } from 'react-chartjs-2';
import { COLOR } from '../constants';
import Request from '../Request'

const { height, width } = Dimensions.get('window');
const data = {
  labels: [
    'Bạn',
    'Team',
  ],
  datasets: [{
    data: [250, 1500],
    backgroundColor: [
      '#FF6384',
      '#36A2EB',
    ],
    hoverBackgroundColor: [
      '#FF6384',
      '#36A2EB',
    ]
  }]
};

const dataBar = {
  labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13'],
  datasets: [
    {
      label: 'Team AppotaPay',
      backgroundColor: "#BB65FF",
      borderColor: COLOR.MAIN,
      borderWidth: 1,
      hoverBackgroundColor: 'green',
      hoverBorderColor: 'yellow',
      data: [65, 59, 80, 81, 56, 55, 40, 0, 16, 22, 35, 65, 42]
    }
  ]
};

class Transaction extends React.Component {
  state = {
    isLoading: true,
  }


  componentDidMount = () => {
    this.getAll()
    this.initInterval = setInterval(() => {
      if (window.Appota) {
        AppotaSDK.init({ title: 'Chi tiết khảo sát' })
        clearInterval(this.initInterval)
      }
    }, 50);
  }

  getAll = async () => {
    const data = {
      poll_id: ''
    }
    let res = await Request.get({ url: "v1/poll/result", data });
    let json = res ? await res.json() : "";
    if (!json) return
    return json;
  }


  render() {
    return (
      <View style={{ flex: 1, width, minHeight: height, display: 'flex', flexWrap: 'wrap', backgroundColor: COLOR.BG, padding: 15 }}>
        <Bar 
        data = {dataBar}
        height={height - 100}/>
      </View >
    )
  }
}
export default withRouter(Transaction)
const styles = StyleSheet.create({
});
