import React from 'react';
import { View, StyleSheet, Dimensions, Text, Image, FlatList, TouchableOpacity } from 'react-native-web';
import { AppotaSDK } from '../Appota-platform';
import { withRouter } from 'react-router-dom';
import { COLOR } from '../constants'
import ListItem from './component/ListItem';
import Popup from './component/Popup';
import { Example } from '../stores'
import Request from '../Request'
const { height, width } = Dimensions.get('window');

class Main extends React.Component {
  state = {
    showPopup: false,
    des: false,
    data: []
  }

  data = [
    { avatar: require('../assets/a1.jpg'), user_name: 'Tran ', time: 4, money: 200, phone_number: '0000001' },
    { avatar: require('../assets/a1.jpg'), user_name: 'Tran ', time: 3, money: 150, phone_number: '0000002' },
    { avatar: require('../assets/a1.jpg'), user_name: 'Tran Va', time: 2, money: 100, phone_number: '0000003' },
    { avatar: require('../assets/a1.jpg'), user_name: 'Tran Van', time: 1, money: 50, phone_number: '0000004' },
    { avatar: require('../assets/a1.jpg'), user_name: 'Tran V', time: 1, money: 50, phone_number: '0000004' },
    { avatar: require('../assets/a1.jpg'), user_name: 'Tran Va', time: 1, money: 50, phone_number: '0000004' },
    { avatar: require('../assets/a1.jpg'), user_name: 'Tran Va', time: 1, money: 50, phone_number: '0000004' },
    { avatar: require('../assets/a1.jpg'), user_name: 'Tran Van', time: 1, money: 50, phone_number: '0000004' },
    { avatar: require('../assets/a1.jpg'), user_name: 'Tran Vag 4', time: 1, money: 50, phone_number: '0000004' },
    { avatar: require('../assets/a1.jpg'), user_name: ' Thang 4', time: 1, money: 50, phone_number: '0000004' },
  ]
  
  componentDidMount = () => {
    this.getListDashboard()
    this.initInterval = setInterval(() => {
      if (window.Appota) {
        AppotaSDK.init({ title: 'Bảng xếp hạng' })
        this.setState({ isLoading: false }, () => {
          this.getUserInfo()
        })
        clearInterval(this.initInterval)
      }
    }, 50);
    setTimeout(() => {
      this.setState({ showPopup: true })
    }, 1000);
  }

  getListDashboard = async () => {
    const data = {
      sort: this.state.des ? 'desc' : 'asc'
    }
    let res = await Request.get({ url: `/v1/list/user`, data });
    let json = res ? await res.json() : "";
    if (!json) return
    this.setState({data: json.data})
    return json;
  }

  getUserInfo = async () => {
    let data = await AppotaSDK.getUserInfo();
    if (!data) return
    this.getUSIF(data)
  }

  getUSIF = async (user) => {
    const data = {
      phone_number: user.phoneNumber
    }
    let res = await Request.get({ url: `/v1/user/get`, data });
    let json = res ? await res.json() : '';
    if (!json) return
    Example.avatar = json.avatar
    Example.phone_number = json.phoneNumber
    Example.fullname = json.fullname
    return json;
  }

  toDetail = (item, index) => {
    this.props.history.push({
      pathname: `/transaction`, state: { item, index }
    })
  }

  toForm = () => {
    this.props.history.push({
      pathname: `/form`, state: {}
    })
  }

  toAdd = () => {
    this.props.history.push({
      pathname: `/search`, state: {}
    })
  }

  render() {
    var { showPopup, data } = this.state
    if(!data || !data[0]) return <View/>
    const { user_name, time, money, phone_number } = data[0]
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => this.toDetail(data[0], 1)} style={styles.mainTab}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.image} >
              <View style={styles.image1} >
                <Text style={{ fontSize: 30, fontWeight: 'bold', color: COLOR.WHITE }}>1</Text>
              </View>
            </View>
            <View>
              <Text style={{ color: COLOR.WHITE, fontWeight: 'bold', fontSize: 18 }}>
                {user_name ? user_name : ''}
              </Text>
              <Text style={{ color: COLOR.WHITE, fontSize: 15 }}>
                {phone_number ? phone_number : ''}
              </Text>
            </View>
          </View>
          <View style={styles.viewCup}>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text style={styles.textTime}>x{time ? time : 0}</Text>
            </View>
            <Image source={require('../assets/1.png')} style={{ width: 60 * 254 / 184, height: 60, marginHorizontal: 20 }} />
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text style={styles.textMoney}>${money ? money : 0}</Text>
            </View>
          </View>
          <TouchableOpacity onPress = {() => this.setState({des: !this.state.des}, () => this.getListDashboard())} style = {{position: 'absolute', top: 0, right: 0}}>
            <Image style={{ width: 20, height: 20, margin: 10 }} source={require('../assets/sort.png')} />
          </TouchableOpacity>
        </TouchableOpacity>


        <View style={styles.viewList}>
          <View style={styles.viewQuote}>
            <Text style={{ color: COLOR.WHITE, fontSize: 15, fontWeight: 'bold' }}>Có tất cả nhưng chẳng có anh!</Text>
          </View>

          <View style={{ flex: 1, paddingHorizontal: 15, marginBottom: 10 }}>
            <FlatList
              data={data.slice(1)}
              keyExtractor={(item, index) => `main${index}`}
              renderItem={({ item, index }) => <ListItem index={index} item={item} onPress={() => this.toDetail(item, index + 2)} />}
            />
          </View>
        </View>
        <TouchableOpacity onPress={this.toAdd} style={{ position: 'absolute', right: 20, top: height - 100, padding: 10 }}>
          <Image source={require('../assets/add.png')} style={{ width: 60, height: 60 }} />
        </TouchableOpacity>
        {/* <TouchableOpacity onPress={this.toForm} style={{ position: 'absolute', right: 20, top: height - 100, padding: 15 }}>
        <Image source={require('../assets/merge.png')} style={{ width: 50, height: 50 }} />
        </TouchableOpacity> */}
        {showPopup ? <Popup data={data[0]} onClose={() => this.setState({ showPopup: false })} /> : <View />}
      </View>
    )
  }
}
export default withRouter(Main)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.BG,
    width,
    minHeight: height,
    display: 'flex',
    flexWrap: 'wrap'
  },
  mainTab: {
    backgroundColor: COLOR.MAIN_TAB,
    borderRadius: 15,
    marginHorizontal: 15,
    marginTop: 20,
    paddingTop: 20,
    paddingBottom: 30,
    paddingHorizontal: 15
  },
  image: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginRight: 20,
    backgroundColor: COLOR.MAIN,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewCup: {
    justifyContent: 'center',
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%'
  },
  textTime: {
    color: COLOR.MAIN,
    fontSize: 24,
    fontWeight: 'bold',
    justifyContent: 'flex-end'
  },
  textMoney: {
    flex: 1,
    color: '#EFE951',
    fontSize: 22,
    fontWeight: 'bold'
  },
  viewList: {
    marginHorizontal: 25,
    borderRadius: 15,
    flex: 1,
    marginBottom: 10,
    marginTop: -10,
    backgroundColor: COLOR.WHITE,
    alignItems: 'center'
  },
  viewQuote: {
    backgroundColor: COLOR.ORANGE,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 15,
    marginTop: -10,
    marginBottom: 20
  }



});
