import React from 'react';
import { View, StyleSheet, Dimensions, Text, Image, TouchableOpacity, TextInput } from 'react-native-web';
import { AppotaSDK } from '../Appota-platform';
import { withRouter } from 'react-router-dom';
import { COLOR } from '../constants'
import ListItem from './component/ListItem';
import Popup from './component/Popup';
import { Example } from '../stores';
import Request from '../Request'

const { height, width } = Dimensions.get('window');

class Main extends React.Component {
  state = {
    phone_number: '',
    user_name: "",
    isEdit: false,
    data: []
  }

  componentDidMount = () => {
    // setup SDK;
    let { state } = this.props.history.location
    if (state && state.isEdit) this.setState({ isEdit: state.isEdit })
    if (state && state.data) this.setState({ phone_number: state.data.phone_number, user_name: state.data.user_name })
    this.initInterval = setInterval(() => {
      if (window.Appota) {
        AppotaSDK.init({ title: state && state.isEdit ? 'Thay đổi thông tin' : 'Thêm thành viên' })
        clearInterval(this.initInterval)
      }
    }, 50);
  }

  sendRequest = async () => {
    const { phone_number, user_name, isEdit } = this.state
    if (!phone_number || phone_number.length != 10 || !Number(phone_number)) {
      alert('Số điện thoại không hợp lệ')
      return
    }
    const data = {
      username: user_name, user_id: ''
    }
    let res = await Request.post({ url: isEdit ? `/v1/user/edit` : "/v1/user/create", data });
    let json = res ? await res.json() : "";
    if (!json) return
    if (json.success) this.props.history.push(`/`)
    return json;
  }

  render() {
    const { phone_number, user_name, isEdit } = this.state
    return (
      <View style={{ flex: 1, backgroundColor: COLOR.BG, width, minHeight: height, display: 'flex', flexWrap: 'wrap', paddingHorizontal: 15 }}>
        <TextInput
          style={{ marginHorizontal: 20, marginVertical: 20, marginTop: 50, paddingVertical: 10, paddingHorizontal: 20, borderRadius: 20, backgroundColor: COLOR.WHITE }}
          onChangeText={user_name => this.setState({ user_name })}
          value={user_name}
          placeholder='Tên thành viên'
        />
        {!isEdit ?<TextInput
          style={{ marginHorizontal: 20, marginVertical: 20, paddingVertical: 10, paddingHorizontal: 20, borderRadius: 20, backgroundColor: COLOR.WHITE }}
          onChangeText={phone_number => this.setState({ phone_number })}
          value={phone_number}
          placeholder='Số điện thoại'
          keyboardType="phone-pad"
        /> : <View/>}
        <View style={{ flex: 1 }} />
        <TouchableOpacity onPress={this.sendRequest} style={{ paddingVertical: 10, borderRadius: 15, margin: 10, alignItems: 'center', backgroundColor: COLOR.RED }}>
          <Text style={{ color: COLOR.WHITE, fontSize: 16, fontWeight: 'bold' }}>{isEdit ? "Thay đổi" : "Thêm thành viên"}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
export default withRouter(Main)
const styles = StyleSheet.create({
});
