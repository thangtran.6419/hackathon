import React from 'react';
import { View, StyleSheet, Dimensions, Text, Animated, Image, Easing, PanResponder } from 'react-native-web';
import { AppotaSDK } from '../Appota-platform';
import { withRouter } from 'react-router-dom';

const { height, width } = Dimensions.get('window');
const arr = Array.from(new Array(500), (x, i) => i + 1)
let CIRCLE_RADIUS = 30;

class Draggable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showDraggable: true,
      dropAreaValues: null,
      pan: new Animated.ValueXY(),
      opacity: new Animated.Value(1)
    };
  }

  componentWillMount() {
    // Add a listener for the delta value change
    this._val = { x: 0, y: 0 }
    this.state.pan.addListener((value) => this._val = value);
    // Initialize PanResponder with move handling
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (e, gesture) => true,
      onPanResponderMove: Animated.event([
        null, { dx: this.state.pan.x, dy: this.state.pan.y }
      ], this.state.pan.setValue({ x: 0, y: 0 })),
      // adjusting delta value
      // this.state.pan.setValue({ x:0, y:0})
      onPanResponderRelease: (e, gesture) => {
        if (this.isDropArea(gesture)) {
          Animated.timing(this.state.opacity, {
            toValue: 0,
            duration: 1000
          }).start(() =>
            this.setState({
              showDraggable: false
            })
          );
        } else {
          Animated.spring(this.state.pan, {
            toValue: { x: 0, y: 0 },
            friction: 5
          }).start();
        }
      }
    });
  }

  isDropArea(gesture) {
    return gesture.moveY < 200;
  }
  render() {
    const panStyle = {
      transform: this.state.pan.getTranslateTransform()
    }

    return (
      <Animated.View
        {...this.panResponder.panHandlers}
        style={[panStyle, styles.circle, { opacity: this.state.opacity }]}
      />
    );
  }
}

class Transaction extends React.Component {
  constructor() {
    super()
    this.spinValue = new Animated.Value(0)
    this.springValue = new Animated.Value(0.3)
    this.animatedValue = []
    arr.map(val => this.animatedValue[val] = new Animated.Value(0))
  }
  componentWillMount() {
    setTimeout(() => {
      this.props.history.push('/main')
    }, 2000);
  }
  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.dropZone}>
          <Text style={styles.text}>Drop them here!</Text>
        </View>
        <View style={styles.ballContainer} />
        <View style={styles.row}>
          <Draggable />
          <Draggable />
          <Draggable />
          <Draggable />
          <Draggable />
        </View>
      </View>
    );
  }
}
export default withRouter(Transaction)

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1
  },
  ballContainer: {
    height: 200
  },
  row: {
    flexDirection: "row"
  },
  dropZone: {
    height: 200,
    backgroundColor: "#00334d"
  },
  text: {
    marginTop: 25,
    marginLeft: 5,
    marginRight: 5,
    textAlign: "center",
    color: "#fff",
    fontSize: 25,
    fontWeight: "bold"
  },
  circle: {
    backgroundColor: "skyblue",
    width: CIRCLE_RADIUS * 2,
    height: CIRCLE_RADIUS * 2,
    borderRadius: CIRCLE_RADIUS
  }
});
