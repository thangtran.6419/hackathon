import React from 'react';
import { View, StyleSheet, Dimensions, Text, Image, FlatList, TouchableOpacity } from 'react-native-web';
import { withRouter } from 'react-router-dom';
import { COLOR } from '../../constants'
import { Example } from '../../stores'
const { height, width } = Dimensions.get('window');

class Main extends React.Component {
  state = {
    isLoading: true,
  }

  componentDidMount = () => {
    // setTimeout(() => {
    //   this.props.history.goBack()
    // }, 2000);
  }

  toDetail = (item) => {
    this.props.history.push({
      pathname: `/transaction`, state: { item }
    })
  }

  render() {
    const { avatar, user_name, time, money, phone_number } = this.props.data
    return (
      <TouchableOpacity onPress = {this.props.onClose} style={{ justifyContent: 'center', flex: 1, backgroundColor: 'rgba(0,0,0,0.7)', width, minHeight: height, display: 'flex', flexWrap: 'wrap', position: 'absolute', top: 0, left: 0 }}>
        <View style={{ marginHorizontal: 15, padding: 20, borderRadius: 20, backgroundColor: COLOR.WHITE, alignItems: 'center', justifyContent: 'center' }}>
          <Image source={require('../../assets/bg.png')} style={{ width: '100%', height: 60 }} />
          <Image source={require('../../assets/1.png')} style={{ width: 60 * 254 / 184, height: 60, borderRadius: 40, marginTop: -15 }} />
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: COLOR.RED, marginVertical: 10 }}>{user_name}</Text>
          <Text style={{ fontSize: 15, color: COLOR.MAIN_TEXT }}>{phone_number}</Text>
          <View style={{ backgroundColor: '#F8B917', borderRadius: 20, marginTop: 20, paddingHorizontal: 15, paddingVertical: 5 }}>
            <Text style={{ color: COLOR.WHITE, fontSize: 13, fontWeight: 'bold' }}>Em mượn rượu tỏ tình đấy thì sao nào!</Text>
          </View>
          <Image source={require('../../assets/fw1.png')} style={{ width: 60, height: 60, position: 'absolute', left: 15, bottom: 50 }} />
          <Image source={require('../../assets/fw.png')} style={{ width: 50, height: 50, position: 'absolute', right: 15, top: 90 }} />
          <Image source={require('../../assets/fe2.png')} style={{ width: 50, height: 50, position: 'absolute', right: 35, top: 110 }} />
        </View>
      </TouchableOpacity>
    )
  }
}
export default withRouter(Main)
const styles = StyleSheet.create({
});
