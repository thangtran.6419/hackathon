import React from 'react';
import { View, StyleSheet, Dimensions, Text, Image, TouchableOpacity } from 'react-native-web';
import { withRouter } from 'react-router-dom';
import { COLOR } from '../../constants'

const { height, width } = Dimensions.get('window');

class Transaction extends React.Component {
  state = {
    isLoading: true,
  }

  render() {
    const { avatar, user_name, time, money, phone_number } = this.props.item
    return (
      <TouchableOpacity onPress={this.props.onPress} style={{ borderBottomWidth: 0.05, borderColor: COLOR.LINE, flexDirection: 'row', alignItems: 'center', marginBottom: 10, paddingBottom: 10, width: width - 80 }}>
        <View style={styles.image} >
          <Text style={{ fontSize: 18, fontWeight: 'bold', color: COLOR.MAIN_TEXT }}>{this.props.index + 2}</Text>
        </View>
        <View style={{ flex: 1 }}>
          <Text style={{ color: "#0872D4", fontWeight: 'bold', fontSize: 15 }}>{user_name}</Text>
          <Text style={{ color: COLOR.MAIN_TEXT, fontSize: 13 }}>{phone_number}</Text>
        </View>
        <Text style={{ color: COLOR.MAIN, fontSize: 15, marginHorizontal: 10, width: 30 }}>x{time}</Text>
        <Text style={{ color: "#FF9900", fontSize: 15, width: 40 }}>${money}</Text>
      </TouchableOpacity>
    )
  }
}

export default withRouter(Transaction)
const styles = StyleSheet.create({
  image: {
    width: 30,
    height: 30,
    borderRadius: 15,
    marginRight: 20,
    backgroundColor: COLOR.WHITE,
    borderColor: "#7755FF",
    borderWidth: 3,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 3
  },
});
